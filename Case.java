public class Case{
    private boolean estDecouverte;
    private boolean estMarquee;
    private boolean contientUneBombe;

    /** Construction d'une case
     * 
     */
    public Case(){
        this.estDecouverte = false;
        this.estMarquee = false;
        this.contientUneBombe = false;
    }

    /** Permet de rédemarrer une partie.
     * 
     */
    public void reset(){
        this.estDecouverte = false;
        this.estMarquee = false;
        this.contientUneBombe = false;
    }

    /** Permet de poser une bombe sur une case du plateau.
     * 
     */
    public void poseBombe(){
        this.contientUneBombe = true;
    }

    /**Permet de retourner un boolean afin de savoir si la case contient une bombe ou non.
     * 
     * @return (type) boolean
     */
    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }

    /**Permet de retourner un boolean afin de savoir si la case est déjà découverte ou non.
     * 
     * @return (type) boolean
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }

    /** Permet de retourner un boolean afin de savoir si la case est déjà marquée ou non.
     * 
     * @return (type) boolean
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }

    /**Permet de retourner un boolean afin de savoir si la case esr déjà révélée ou non.
     * 
     * @return (type) boolean
     */
    public boolean reveler(){
        if (!this.estDecouverte()){
            this.estDecouverte = true;
            return true;
        }
        return false;
    }
    /**Permet de marquer une case du plateau.
     * 
     */
    public void marquer(){
        this.estMarquee = !this.estMarquee;
    }
}













