import java.util.ArrayList;
import java.util.List;

public class CaseIntelligente extends Case {

    private List<Case> lesVoisines;

    public CaseIntelligente(){
        super();
        lesVoisines = new ArrayList<>();
    }

    /**Permet d'ajouter des cases voisines à une case.
     * 
     * @param uneCase
     */
    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
        }

    /**Permet de retourner le nombre de bombbes voisines.
     * 
     * @return (type) int
     */
    public int nombreBombesVoisines(){
        int cpt = 0;
        for (Case kase : this.lesVoisines){
            if (kase.contientUneBombe()){
                cpt +=1;
            }
        }
        return cpt;
    }

    @Override
    public String toString(){
        if(this.estDecouverte()){
            if(this.contientUneBombe()) return "@";
            else return ""+this.nombreBombesVoisines();
        }else{
            if(this.estMarquee())return "?";              
        }
        return " ";
    }










}