import java.util.Random;
import java.util.ArrayList;
import java.util.List;


public class Plateau {
    private int nbLignes;
    private int nbColonnes;
    private int nbBombes;
    private int pourcentageDeBombes;
    private List<List<CaseIntelligente>> lesCases;


    /**Permet de construire un plateau.
     * 
     * @param nbLignes
     * @param nbColonnes
     * @param nbBombes
     */
    public Plateau(int nbLignes, int nbColonnes, int pourcentage) {
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.creerLesCasesVides();
        this.poseDesBombesAleatoirement();
        this.rendLesCasesIntelligentes();
        
    
    }

    /**Permet de créer des cases vides.
     * 
     */
    private void creerLesCasesVides(){
        this.lesCases = new ArrayList<>();
        for (int i=0; i<nbLignes; i++){
            this.lesCases.add(new ArrayList<>());
            for (int j=0; j<nbColonnes; j++){
                this.lesCases.get(i).add(new CaseIntelligente());
            }
        }
    }

    /**Permet de rendre les cases intelligentes.
     * 
     */
    private void rendLesCasesIntelligentes(){
        for (int i = 0; i<nbLignes; i++){
            this.lesCases.add(new ArrayList<>());
            for (int j = 0; j<nbColonnes; j++){
                this.lesCases.get(i).add(new CaseIntelligente());
                for (int k = i-1; k<=i+1; k++){
                    if(k>=0 && k<this.getNbLignes()){
                        for(int l = j-1; l<=j+1; l++){
                        if(l>=0 && l<this.getNbColonnes()){
                            this.getCase(i, j).ajouteVoisine(this.getCase(k, l));
                        }
                    }
                }
            }
        }
    }
}
    /**Permet de poser des bombes aléatoirement sur des cases.
     * 
     */
    protected void poseDesBombesAleatoirement(){
            Random generateur = new Random();
            for (int x = 0; x < this.getNbLignes(); x++){
                for (int y = 0; y < this.getNbColonnes(); y++){
                    if (generateur.nextInt(100)+1 < this.pourcentageDeBombes){
                        this.poseBombe(x, y);
                        this.nbBombes = this.nbBombes + 1;
                    }
                }
            }
        }


    /**Permet de retourner le nombre de lignes.
     * 
     * @return (type) int
     */
    public int getNbLignes(){
        return this.nbLignes;
    }

    /** Permet de retourner le nombre de colonnes.
     * 
     * @return (type) int
     */
    public int getNbColonnes(){
        return this.nbColonnes;
    }

    /**Permet de retourner le nombre total de bombes.
     * 
     * @return (ype) int
     */
    public int getNbTotalBombes(){
        return this.nbBombes;
    }

    /**Permet de retourner une case intelligente.
     * 
     * @param ligne
     * @param colonne
     * @return
     */
    public CaseIntelligente getCase(int ligne, int colonne){
        if (this.lesCases.size()>ligne && ligne >=0 && this.lesCases.get(0).size()>colonne && colonne >=0)
            return this.lesCases.get(ligne).get(colonne);
        return null;
    }

    /**Permet de retourner le nombre de cases marquées.
     * 
     * @return (type) int
     */
    public int getNbCasesMarquees(){
        int cptmarquee = 0;
        for (List<CaseIntelligente> kase : lesCases){
            for(Case kaase : kase){
                if(kaase.estMarquee()){
                    cptmarquee =+ 1;
                }
            }
        }
        return cptmarquee;
    }

    /**Permet de poser une bombe sur une case.
     * 
     * @param x
     * @param y
     */
    public void poseBombe(int x, int y){
        this.getCase(x,y).poseBombe();
            
    }

    /**Permet de remettre à zéro la le plateau.
     * 
     */
    public void reset(){
        this.nbBombes = 0;
        for (List<CaseIntelligente> rangee : this.lesCases){
            for (Case kase : rangee){
               kase.reset();
            }
        }
    }

}